﻿// ==ClosureCompiler==
// @output_file_name compiled_content.js
// @compilation_level ADVANCED_OPTIMIZATIONS
// @language_in ECMASCRIPT5
// ==/ClosureCompiler==

/*
* Versao em BETA teste
* desenvolvido por Yuri Araujo
*/

var ProjectNetwork = {
    InicialUrl : 0,
    Socket: null,
    Audio: null,
    UserEmail: "",

    /*
    * Inicia todo o funcionamento
    */
    Iniciar: function () {
		ProjectNetwork.RemoverAdsDeVideo();
    },

    RemoverAdsDeVideo: function () {
        var anuncioArmagedon = document.querySelectorAll("iframe[src*='anuncio.php'");
        for (var i = 0; i < anuncioArmagedon.length; i++)
            anuncioArmagedon[i].parentNode.remove();

        var interval = setInterval(function () {
			var anuncioArmagedon = document.querySelectorAll("iframe[src*='anuncio.php'");
			for (var i = 0; i < anuncioArmagedon.length; i++)
			    anuncioArmagedon[i].parentNode.remove();
        }, 500);

        setTimeout(function () {
            clearInterval(interval);
        }, 10000);

    },

    ObterUsuarioLogado : function() {
        ProjectNetwork.Socket.postMessage({ callback: "GetUser" });
    },

    /*
    * Instala o Analytic na pagina do youtube
    */
    NotificarAnalytic: function (action, msg) {
        var sender = { action: action, body: msg };
        //ProjectNetwork.Socket.postMessage({ callback: "GA", msg: sender });
    },

    /*
    * Adiciona ouvinte para executar acoes delegada da ENGINE
    */
    AdicionarOuvinte: function () {
        ProjectNetwork.Socket = chrome.runtime.connect({ name: "knockknock" });
        ProjectNetwork.Socket.postMessage({ status: 200, mensagem: "Conectado com sucesso" });

        ProjectNetwork.Socket.onMessage.addListener(function (msg) {
            console.log(msg);
            if (msg.callback == undefined) return;

            switch (msg.callback) {
                case "AutoReplay":
                    //ProjectNetwork.AutoReplay.Toggle();
                    break;
                case "GetUser":
                    UserEmail = msg.user;
                default:
            }
        });
    },

    /*
    * Insere o CSS da extension
    */
    InserirStyleSheet: function () {
        var link = document.createElement('link');
        link.href = chrome.extension.getURL('css/armagedon.css');
        link.id = 'cssPN';
        link.type = 'text/css';
        link.rel = 'stylesheet';
        document.head.appendChild(link);
    },

    
    /*
    * Remove os videos de anuncio e avanca para o video do usuario
    */
    ExecutarClick: function(elemento) {
        if (typeof elemento == "object" && elemento != null) {
            var ev = document.createEvent("MouseEvents");
            ev.initMouseEvent("click", true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 2, null);
            elemento.dispatchEvent(ev);
        }
        else if (typeof elemento == "string") {
            var ev = document.createEvent("MouseEvents");
            ev.initMouseEvent("click", true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 2, null);
            var btn = document.querySelector(elemento);

            if(btn != null)
                btn.dispatchEvent(ev);
        }
    },

    DB: {
        Open: function () {
            var db = openDatabase("YoutubeTools", "1.0", "", 200000);
            db.transaction(function (query) {
                query.executeSql("CREATE TABLE IF NOT EXISTS WatchConfig(id TEXT, inicio TEXT,fim TEXT)");
            });

            return db;
        },

        SetVideoConfig: function (id, inicio, fim) {
            var db = this.Open();

            db.transaction(function (query) {
                query.executeSql("SELECT * FROM WatchConfig WHERE id=?", [id],
                function (data1, result) {
                    if (result.rows.length == 0) {
                        db.transaction(function (query) {
                            query.executeSql("INSERT INTO WatchConfig (id,inicio,fim) VALUES (?,?,?)", [id, inicio, fim],
                                function (query2, resultado) {
                                }, function (query2, resultado) {
                                    console.log(query2);
                                    console.log(resultado);
                                })
                        });
                    } else {
                        query.executeSql("UPDATE WatchConfig SET inicio=?, fim=? WHERE id=?", [inicio, fim, id], null, null);
                    }
                }
                )
            });

        },

        DeleteVideoConfig: function (id, inicio, fim) {
            var db = this.Open();

            db.transaction(function (query) {
                query.executeSql("DELETE FROM WatchConfig WHERE id=?", [id], null, null);
            });
        },

    }

}

ProjectNetwork.Iniciar();