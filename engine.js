// ==ClosureCompiler==
// @output_file_name compiled_engine.js
// @compilation_level ADVANCED_OPTIMIZATIONS
// @language_in ECMASCRIPT5
// ==/ClosureCompiler==

var socket = null;
var Usuario;

_gaq = [];
var UA = "UA-46699996-4";

_gaq.push(['_setAccount', UA]);
_gaq.push(['_trackPageview']);

// Standard Google Universal Analytics code
(function () {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = 'https://ssl.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();

chrome.tabs.onUpdated.addListener(function (tabId, info, tab) {
    var listaScripts = chrome.runtime.getManifest().content_scripts
    var isSupported = false;
    for (var i = 0; i < listaScripts.length; i++) {
        for (var c = 0; c < listaScripts[i].matches.length; c++) {
            if (tab.url.search(listaScripts[i].matches[c]) > -1)
                isSupported = true;

        }
    }

    if (isSupported) 
        chrome.browserAction.enabled(tabId)
    else 
        chrome.browserAction.disable(tabId)

});

chrome.browserAction.onClicked.addListener(function (callback) {
    console.log({ "socket": socket });

    if (socket == null) {
        chrome.tabs.query({ url: "http://*/*" }, function (tabs) {
            debugger
            for (var q = 0; q < tabs.length; q++) {
                var listaScripts = chrome.runtime.getManifest().content_scripts
                for (var i = 0; i < listaScripts.length; i++) {
                    for (var c = 0; c < listaScripts[i].matches.length; c++) {
                        if (tabs[q].url.search(listaScripts[i].matches[c]) > -1)
                            chrome.tabs.executeScript(tabs[q].id, { file: listaScripts[i].js[0] }, function () { });

                    }
                }
            }

            chrome.tabs.query({ url: "https://*/*" }, function (tabs) {
                debugger
                for (var q = 0; q < tabs.length; q++) {
                    var listaScripts = chrome.runtime.getManifest().content_scripts
                    for (var i = 0; i < listaScripts.length; i++) {
                        for (var c = 0; c < listaScripts[i].matches.length; c++) {
                            if (tabs[q].url.search(listaScripts[i].matches[c]) > -1)
                                chrome.tabs.executeScript(tabs[q].id, { file: listaScripts[i].js[0] }, function () { });

                        }
                    }
                }
            });
        });

       
        //Executa o script nas paginas que ja foram carregadas
       

    }

    socket.postMessage({ callback: "AutoReplay" })
});

chrome.runtime.onConnect.addListener(function (port) {
    socket = port;
    port.onMessage.addListener(function (request) {
        console.log(request);

        if (request.status == 200)
            socket.postMessage("Conectado!");

        if (request.callback == "AbrirAlerta")
            AbrirAlerta(request);

        if (request.callback == "VerificarAtualizacao")
            VerificarAtualizacao();

        if (request.callback == "VerificarAtualizacao")
            VerificarAtualizacao();

        if (request.callback == "GetUser")
            ObterUsuario();

        if (request.callback == "GA") {
            request.msg.body.versao = chrome.runtime.getManifest().version;
            request.msg.body.usuario = Usuario.email;

            var sender = request.msg;
            _gaq.push(['_trackEvent', sender.action, JSON.stringify(sender.body)]);
        }

    });
});


chrome.notifications.onButtonClicked.addListener(function(notfiId, btnIndex)
{
    //if (btnIndex == 0)
	//	chrome.tabs.create({ url: chrome.app.getDetails().update_url.toString().replace("update.xml","YoutubeTools.zip") });
});

var NotificarInstalacao = function () {
    if (localStorage.getItem("install") == null) {
        var installData = JSON.stringify({ usuario: Usuario.email, data: new Date() });
        localStorage.setItem("install", installData);

        setTimeout(function () {
            _gaq.push(['_trackEvent', "Instalacao", installData]);
        }, 4000);
    }
};

var ObterUsuario = function () {
    if (Usuario == null) {
        chrome.identity.getProfileUserInfo(function (user) {
            Usuario = user;
            if (socket != null)
                socket.postMessage({ callback: "GetUser", user: user });

            NotificarInstalacao();
        });
    } else {

        if (socket != null)
            socket.postMessage({ callback: "GetUser", user: Usuario });
    }
};

var VerificarAtualizacao = function()
{
	chrome.runtime.requestUpdateCheck(function(status,versao)
	{
		if (status == "update_available") {
		    AtualizarYoutubeTools(versao);
	  } else if (status == "no_update") {
	      console.log("no update found");
	  } else if (status == "throttled") {
	      console.log("Oops, I'm asking too frequently - I need to back off.");
	  }
	});
};

var AtualizarYoutubeTools = function (versao) {
    AbrirAlerta({
        id: "NovaVersao",
        from: "engine",
        title: "Youtube� Tools",
        body: "Ha uma vers�o dispon�vel: " + versao.version.toString(),
        btnList: [{ title: "Iniciar download" }]
    });
};

var AbrirAlerta = function (msg) {

    if (msg.from == undefined) return;
    if (msg.id == undefined) msg.id = msg.from;
    if (msg.body == undefined) return;
    if (msg.title == undefined) return;
    if (msg.btnList == undefined) msg.btnList = [];

    msg.id = msg.id.toString();

    chrome.notifications.clear(msg.id, function (e) {
        console.log(e);
    });

    chrome.notifications.create(msg.id, {
        type: 'basic',
        title: msg.title,
        iconUrl: chrome.extension.getURL('img/icon128.png'),
        message: msg.body,
        priority: 1,
        isClickable: true,
        buttons: msg.btnList
    }, function (id) {
        console.log("callback");
    });
};

ObterUsuario();
